package org.spiralarms.student_admission;

import java.io.Serializable;
import java.util.ArrayList;

public class Faculty extends ObjectWithDateAndName implements Serializable {
    ArrayList<Abiturient> admissions;
    ArrayList<Exam> exams;
    double entryMark;

    public Faculty(String name, ArrayList<Exam> exams, double entryMark, Localizer loc) {
        super(name, loc);
        this.exams = exams;
        this.entryMark = entryMark;
        admissions = new ArrayList<>();
    }

    public void registerAdmission(Abiturient a) {
        admissions.add(a);
        System.out.printf(loc.getLocalizedText("registered_to") + "\n", a.toString(), this.toString());
    }

    @Override
    public String toString() {
        return loc.getLocalizedText("faculty") + " " + super.toString();
    }

    @Override
    public void setLocalizer(Localizer loc) {
        super.setLocalizer(loc);
        for (Abiturient a : admissions) {
            a.setLocalizer(loc);
        }
        for (Exam e : exams) {
            e.setLocalizer(loc);
        }
    }

    public ArrayList<Abiturient> getAdmissions() {
        return admissions;
    }

    public void examinate() {
        for (Abiturient a : admissions) {
            System.out.printf(loc.getLocalizedText("checking_works") + "\n", a);
            for (Exam e : exams) {
                e.examinate(a);
            }
        }

        admissions.removeIf(a -> a.getExamResults().getAverage() < entryMark);

        System.out.printf(loc.getLocalizedText("students_passed_to_faculty_with_entry_mark") + "\n", this.toString(), this.entryMark);
        for (Abiturient a : admissions) {
            System.out.printf(loc.getLocalizedText("to_with_average_mark") + "\n", a, a.getExamResults().getAverage());
        }
    }
}
