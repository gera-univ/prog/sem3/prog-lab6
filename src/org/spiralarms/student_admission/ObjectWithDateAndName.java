package org.spiralarms.student_admission;

import java.io.Serializable;

public class ObjectWithDateAndName extends ObjectWithDate implements Serializable {
    String name;

    public ObjectWithDateAndName(String name, Localizer loc) {
        super(loc);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + " " + super.toString();
    }
}
