package org.spiralarms.student_admission;

import java.io.Serializable;

public class Exam extends ObjectWithDateAndName implements Serializable {
    Tutor tutor;

    public Exam(String name, Tutor tutor, Localizer loc) {
        super(name, loc);
        this.tutor = tutor;
    }

    public void examinate(Abiturient a) {
        ExamResults er = a.getExamResults();
        tutor.rate(er);
        a.setExamResults(er);
    }

    @Override
    public void setLocalizer(Localizer loc) {
        super.setLocalizer(loc);
        tutor.setLocalizer(loc);
    }

    @Override
    public String toString() {
        return loc.getLocalizedText("exam") + " " + super.toString();
    }
}
