package org.spiralarms.student_admission;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Locale;

public class DeserializationTest {
    public static void main(String[] args) {
        try {
            FileInputStream fileIn = new FileInputStream("/tmp/admissions.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Locale l = new Locale("en", "GB");
            Localizer loc = new Localizer(l);
            University u = (University) in.readObject();
            u.setLocalizer(loc);
            u.printAdmittedStudents();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            System.out.println("File not found");
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("University class not found");
            c.printStackTrace();
        }
    }
}
