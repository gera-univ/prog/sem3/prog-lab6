package org.spiralarms.student_admission;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        Locale locale = new Locale("en", "GB");
        if (args.length > 0) {
            if (args[0].equals("--locale") || args.length == 2) {
                StringTokenizer localeTokens = new StringTokenizer(args[1], "_");
                locale = new Locale(localeTokens.nextToken(), localeTokens.nextToken());
            } else if (args[0].equals("--help")) {
                printUsage(0);
            } else {
                printUsage(1);
            }
        }
        Localizer loc = new Localizer(locale);
        Tutor t1 = new Tutor("T1", loc);
        ArrayList<Exam> f1Exams = new ArrayList<>();
        f1Exams.add(new Exam("E1", t1, loc));
        f1Exams.add(new Exam("E1", t1, loc));
        f1Exams.add(new Exam("E1", t1, loc));
        Faculty f1 = new Faculty("F1", f1Exams, 4.0, loc);
        Abiturient a1 = new Abiturient("A1", loc);
        Abiturient a2 = new Abiturient("A2", loc);
        Abiturient a3 = new Abiturient("A3", loc);
        f1.registerAdmission(a1);
        f1.registerAdmission(a2);
        f1.registerAdmission(a3);
        ArrayList<Faculty> u1Faculties = new ArrayList<>();
        u1Faculties.add(f1);
        University u = new University("U1", u1Faculties, loc);
        u.startExams();
        u.printAdmittedStudents();

        try {
            FileOutputStream fileOut =
                    new FileOutputStream("/tmp/admissions.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(u);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            System.out.println(loc.getLocalizedText("io_exception"));
            i.printStackTrace();
        }
    }

    private static void printUsage(int exitStatus) {
        System.out.printf("Usage:\n  java %s\n\nOptions:\n  --help\tprint this message\n  --locale\tspecify a locale to use\n", Main.class.getCanonicalName());
        System.exit(exitStatus);
    }
}
