package org.spiralarms.student_admission;

import java.io.Serializable;

public class Abiturient extends ObjectWithDateAndName implements Serializable {
    ExamResults examResults;

    public Abiturient(String name, Localizer loc) {
        super(name, loc);
        examResults = new ExamResults(loc);
    }

    public ExamResults getExamResults() {
        return examResults;
    }

    public void setExamResults(ExamResults examResults) {
        this.examResults = examResults;
    }

    @Override
    public void setLocalizer(Localizer loc) {
        super.setLocalizer(loc);
        examResults.setLocalizer(loc);
    }

    @Override
    public String toString() {
        return loc.getLocalizedText("abiturient") + " " + super.toString();
    }
}
