package org.spiralarms.student_admission;

import java.io.Serializable;
import java.util.ArrayList;

public class ExamResults extends ObjectWithDate implements Serializable {
    private final ArrayList<Integer> marks = new ArrayList<>();

    public ExamResults(Localizer loc) {
        super(loc);
    }

    public void addExamResult(Integer mark) {
        marks.add(mark);
    }

    public double getAverage() {
        return marks.stream().mapToDouble(a -> a).sum() / marks.size();
    }
}
