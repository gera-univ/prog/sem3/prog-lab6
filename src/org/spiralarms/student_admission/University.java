package org.spiralarms.student_admission;

import java.io.Serializable;
import java.util.ArrayList;

public class University extends ObjectWithDateAndName implements Serializable {
    ArrayList<Faculty> faculties;

    public University(String name, ArrayList<Faculty> faculties, Localizer loc) {
        super(name, loc);
        this.faculties = faculties;
    }

    public void printAdmittedStudents() {
        System.out.printf(loc.getLocalizedText("students_admitted_to") + ":\n", this);
        for (Faculty f : faculties) {
            for (Abiturient a : f.getAdmissions()) {
                System.out.printf(loc.getLocalizedText("to_a") + "\n", a, f);
            }
        }
    }

    public void startExams() {
        for (Faculty f : faculties) {
            f.examinate();
        }
    }

    @Override
    public void setLocalizer(Localizer loc) {
        super.setLocalizer(loc);
        for (Faculty f : faculties) {
            f.setLocalizer(loc);
        }
    }

    @Override
    public String toString() {
        return loc.getLocalizedText("university") + " " + super.toString();
    }
}