package org.spiralarms.student_admission;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

public class ObjectWithDate implements Serializable {
    transient protected Localizer loc;
    private final Date creationDate;

    public ObjectWithDate(Localizer loc) {
        this.loc = loc;
        creationDate = new Date();
    }

    public Localizer getLocalizer() {
        return loc;
    }

    public void setLocalizer(Localizer loc) {
        this.loc = loc;
    }

    @Override
    public String toString() {
        DateFormat df = DateFormat.getTimeInstance(DateFormat.FULL, loc.getLocale());

        return "{" +
                loc.getLocalizedText("creation_date") + ": " + df.format(creationDate) +
                '}';
    }
}
