package org.spiralarms.student_admission;

import java.io.Serializable;

public class Tutor extends ObjectWithDateAndName implements Serializable {
    public Tutor(String name, Localizer loc) {
        super(name, loc);
    }

    public void rate(ExamResults e) {
        int randomMark = (int) (Math.random() * (11));
        e.addExamResult(randomMark);

        System.out.printf(loc.getLocalizedText("has_put_mark") + "\n", this, randomMark);
    }

    @Override
    public String toString() {
        return loc.getLocalizedText("tutor") + " " + super.toString();
    }
}
